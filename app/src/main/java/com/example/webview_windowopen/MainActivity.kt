package com.example.webview_windowopen

import android.os.Build
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    lateinit var mContainer : FrameLayout
    lateinit var mWebViewPop : WebView
    lateinit var myWebView : WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview)
        WebView.setWebContentsDebuggingEnabled(true)
        mContainer = findViewById(R.id.webview_frame)
        mWebViewPop = findViewById(R.id.webview)
        myWebView = findViewById(R.id.webview)
        myWebView.loadUrl("file:///android_asset/webViewTest.html")
        myWebView.apply {
            webViewClient = WebViewClient()
            settings.javaScriptEnabled = true
            settings.javaScriptCanOpenWindowsAutomatically = true
            settings.setSupportMultipleWindows(true)
        }

        myWebView.webChromeClient = object : WebChromeClient(){
            override fun onCreateWindow(view: WebView?, isDialog: Boolean, isUserGesture: Boolean, resultMsg: Message?): Boolean {
                Toast.makeText(this@MainActivity, "onCreateWindow", Toast.LENGTH_SHORT).show()
                mWebViewPop = WebView(this@MainActivity).apply {
                    webViewClient = WebViewClient()
                    settings.javaScriptEnabled = true
                }
                mContainer.addView(mWebViewPop)

                mWebViewPop.webChromeClient = object : WebChromeClient() {
                    override fun onCloseWindow(window: WebView?) {
                        mContainer!!.removeView(window)
                        window!!.destroy()
                    }
                }
                (resultMsg?.obj as WebView.WebViewTransport).webView = mWebViewPop
                resultMsg.sendToTarget()
                return true
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBackPressed() {
        //웹사이트에서 뒤로 갈 페이지 존재시
        if(mWebViewPop.canGoBack() && mContainer.childCount>=2) {
            Toast.makeText(this, "back", Toast.LENGTH_SHORT).show()
            mWebViewPop.goBack() // 웹사이트 뒤로가기
        }
        else if(!mWebViewPop.canGoBack() && mContainer.childCount>=2 ){
            Toast.makeText(this, "onCloseWindow", Toast.LENGTH_SHORT).show()
            mWebViewPop.webChromeClient?.onCloseWindow(mWebViewPop) //onCloseWindow
        }
        else{
            Toast.makeText(this, "webview close", Toast.LENGTH_SHORT).show()
            super.onBackPressed() //webview 종료
        }
    }
}